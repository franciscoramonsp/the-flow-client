import { Component, OnInit } from '@angular/core';
import { News } from './news';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})

export class NewsComponent implements OnInit {
  news: Array<News> = [
    {
      title: 'Test',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo ante auctor ligula tincidunt faucibus. Morbi porta dui sit amet metus varius tempus. Fusce id tempor odio. Suspendisse potenti. Phasellus maximus dui nibh, facilisis sodales mauris tincidunt dapibus. Curabitur semper tempor nibh ac pharetra. Vestibulum ut ipsum vestibulum, suscipit eros a, mollis velit. Nunc viverra, neque eu egestas congue, nisl nibh blandit velit, non accumsan turpis nibh nec neque. Vestibulum vitae neque in velit dapibus finibus varius cursus tellus. In vel massa ut tortor ultricies tempor posuere nec augue. Fusce eget consequat ante. Aliquam sollicitudin mauris ac justo vestibulum consequat. Nullam vel velit quis ante imperdiet eleifend at vitae elit. Curabitur facilisis porta metus, non finibus mi suscipit mollis. Donec quis erat eget sapien gravida ullamcorper. Pellentesque fermentum mi ac posuere venenatis.',
      author: 'Francisco Ramon [franciscoramonsp@outlook.com]'
    },
    {
      title: 'Test',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo ante auctor ligula tincidunt faucibus. Morbi porta dui sit amet metus varius tempus. Fusce id tempor odio. Suspendisse potenti. Phasellus maximus dui nibh, facilisis sodales mauris tincidunt dapibus. Curabitur semper tempor nibh ac pharetra. Vestibulum ut ipsum vestibulum, suscipit eros a, mollis velit. Nunc viverra, neque eu egestas congue, nisl nibh blandit velit, non accumsan turpis nibh nec neque. Vestibulum vitae neque in velit dapibus finibus varius cursus tellus. In vel massa ut tortor ultricies tempor posuere nec augue. Fusce eget consequat ante. Aliquam sollicitudin mauris ac justo vestibulum consequat. Nullam vel velit quis ante imperdiet eleifend at vitae elit. Curabitur facilisis porta metus, non finibus mi suscipit mollis. Donec quis erat eget sapien gravida ullamcorper. Pellentesque fermentum mi ac posuere venenatis.',
      author: 'Francisco Ramon [franciscoramonsp@outlook.com]'
    },
    {
      title: 'Test',
      body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris commodo ante auctor ligula tincidunt faucibus. Morbi porta dui sit amet metus varius tempus. Fusce id tempor odio. Suspendisse potenti. Phasellus maximus dui nibh, facilisis sodales mauris tincidunt dapibus. Curabitur semper tempor nibh ac pharetra. Vestibulum ut ipsum vestibulum, suscipit eros a, mollis velit. Nunc viverra, neque eu egestas congue, nisl nibh blandit velit, non accumsan turpis nibh nec neque. Vestibulum vitae neque in velit dapibus finibus varius cursus tellus. In vel massa ut tortor ultricies tempor posuere nec augue. Fusce eget consequat ante. Aliquam sollicitudin mauris ac justo vestibulum consequat. Nullam vel velit quis ante imperdiet eleifend at vitae elit. Curabitur facilisis porta metus, non finibus mi suscipit mollis. Donec quis erat eget sapien gravida ullamcorper. Pellentesque fermentum mi ac posuere venenatis.',
      author: 'Francisco Ramon [franciscoramonsp@outlook.com]'
    }
  ];

  constructor() {}
  ngOnInit() {}

}
