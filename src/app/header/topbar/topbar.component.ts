import { Component, OnInit } from '@angular/core';
import {MatToolbarRow} from '@angular/material';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent extends MatToolbarRow implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
