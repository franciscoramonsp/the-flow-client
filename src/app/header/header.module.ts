import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopbarComponent } from './topbar/topbar.component';
import { HeaderComponent } from './header.component';

import {
  MatToolbarModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule
} from '@angular/material';

import { BotbarComponent } from './botbar/botbar.component';
import { SearchComponent } from './search/search.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule
  ],
  declarations: [TopbarComponent, HeaderComponent, BotbarComponent, SearchComponent, UserSettingsComponent],
  exports: [
    HeaderComponent
  ]
})
/**
 * Wrapper module to create all the components used in the `Header`
 * */
export class HeaderModule { }
