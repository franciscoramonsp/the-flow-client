import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import { NewsComponent } from './news/news.component';
import { MatCardModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule
  ],
  exports: [
    ContentComponent
  ],
  declarations: [ContentComponent, NewsComponent]
})
export class ContentModule { }
