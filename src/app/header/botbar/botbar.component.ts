import { Component, OnInit } from '@angular/core';
import {MatToolbarRow} from '@angular/material';

@Component({
  selector: 'app-botbar',
  templateUrl: './botbar.component.html',
  styleUrls: ['./botbar.component.scss']
})
export class BotbarComponent extends MatToolbarRow implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
