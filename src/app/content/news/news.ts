export interface News {
  title: string;
  body: string;
  author: string;
}
